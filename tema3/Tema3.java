// Bobin Marius - 341

import java.math.*;
import java.util.*;

public class Tema3 {

    static int BANANA = 50; // for scale
    static double EPSILON = 1e-10;

    public static void printMatrix(double[][] matrix, int n) {
        for (int i = 0; i < n; i++) {
          for (int j = 0; j < n; j++) {
            System.out.print(matrix[i][j] + " ");
          }
          System.out.println();
        }
    }

    public static double[][] generateMatrix(int n){

			double[][] matrix = new double[n][n];

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
						matrix[i][j] = 0.0;
				}
			}

			for (int i = 0; i < n; i++) {
				matrix[i][i] = 2.0;
				if(i < n-1) {	
					matrix[i][i+1] = 1.0;
					matrix[i+1][i] = 1.0;
				}
			}
			return matrix;
		}

		public static double[] generateVector(int n){

			double[] b = new double[n];
			for (int i = 0; i < n; i++)
        b[i] = 1;
      return b;
		}

  	public static void verificaSolutie(double A[][], double x[], double b[], int n){
  	
	  	System.out.println("\n------ Verificare Solutie ------\n");
	  	for (int i = 0; i < n; i++){
	  	 	double sum = 0.0;
	  	 	for(int j = 0; j < n; j++){
	  	 		sum += A[i][j] * x[j];
	  	 	}
	  	 	sum -= b[i];
	  	 	System.out.println(i + ": " + sum);
	  	 } 
	  		System.out.println("\n------ Verificare END ------\n");
	  }

	  public static void afisareSolutie(double[] x, int n){

	  	System.out.println("---------- Solutia -----------\n");
      for (int i = 0; i < n; i++) {
        System.out.println("X[" + i + "]: " + x[i]);
      }
      System.out.println("\n---------- Solutie END -----------\n"); 
	  }

    public static double norma(double[] y, double[] x, int n){
        double sum = 0.0;
        for (int i = 0; i < n; i++)
            sum += (y[i] - x[i]) * (y[i] - x[i]);
        return Math.sqrt(sum);
    }
    
    public static double produsVectori(double[] x, double[] y, int n){
        double sum = 0;
        for (int i = 0; i < n; i++)
            sum += x[i] * y[i];
        return sum;
    }
    

	// Metodele

    public static void Jacobi(double[][] matrix, double[] b, int n, int p) {

        double ni = 3.0;
        double sigma = 0.0;
        double[][] Bsigma = new double[n][n];
        double[] bsigma = new double[n];
        
        int nr_iteratii = 0;
        double sigma_optim = 0.0;
        double[] x = new double[n];

        for (int k = 1; k <= p; k++) {
          sigma = (2 * k) / ((p + 1) * ni);
          for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
              if (i == j)
                Bsigma[i][j] = 1.0 - sigma * matrix[i][i];
              else
                Bsigma[i][j] = -1.0 * (sigma * matrix[i][j]);
            }
          }
          for (int i = 0; i < n; i++)
            bsigma[i] = b[i] * sigma;

          double[] xi = new double[n];
          double[] xi1  = new double[n];
          
          for (int i = 0; i < n; i++)
            xi[i] = 0;
          
          int m = 0;
          double ERROR = 0.0;

          do 
          {
            m++;

            for (int i = 0; i < n; i++){
              xi1[i] = 0.0;
              for (int j = 0; j < n; j++){
                xi1[i] += Bsigma[i][j] * xi[j];
              }
              xi1[i] += bsigma[i];
            }


            double sum = 0.0;
            for (int i = 0; i < n; i++){
              for (int j = 0; j < n; j++){
                sum += matrix[i][j] * (xi1[j] - xi[j]) * (xi1[i] - xi[i]);
              }
            }
            
            ERROR = Math.sqrt(sum);

            System.arraycopy(xi1, 0, xi, 0, n);

          } while (ERROR > EPSILON);
            
            
          if (k == 1) {
            nr_iteratii = m;
            sigma_optim = sigma;
            System.arraycopy(xi, 0, x, 0, n);
          }
          
          if (k > 1 && m < nr_iteratii) {
            sigma_optim = sigma;
            nr_iteratii = m;
            System.arraycopy(xi, 0, x, 0, n);
          }
        }

        System.out.println("\n--------- Metoda Jacobi --------- \n");
        System.out.println("Parametrul optim: " + sigma_optim);
        System.out.println("Iteratii: " + nr_iteratii);

        afisareSolutie(x, n);
        verificaSolutie(matrix, x, b, n);
    }


    public static void GaussSeidel(double[][] matrix, double[] b, int n, int p) {
        
        double[] xi = new double[n];
        double[] y = new double[n];
        double[] x = new double[n];

        double ERROR, sigma = 0.0, sigma_optim = 0.0;
        int m, nr_iteratii = 0;

        for (int k = 1; k <= p; k++) {
            sigma = (double) (2 * k) / (p + 1);
            
            m = 0;
            for (int i = 0; i < n; i++) {
                xi[i] = 0;
            }

            do
            {
              m++;
                
              double sum = 0.0;
              for (int i = 0; i < n; i++) {
                sum = (1 - sigma) * xi[i];
                  double sum1 = 0.0;
                  for (int j = 0; j < i; j++) 
                    sum1 += matrix[i][j] * y[j];
                  double sum2 = 0;
                  for (int j = i + 1; j < n; j++)
                    sum2 += matrix[i][j] * xi[j];
                  y[i] = sum + (sigma / matrix[i][i]) * (b[i] - sum1 - sum2);
                }

              sum = 0.0;
              for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                  sum += matrix[i][j] * (y[j] - xi[j]) * (y[i] - xi[i]);
              ERROR = Math.sqrt(sum);
                
							System.arraycopy(y, 0, xi, 0, n);
            } while (ERROR > EPSILON);
            
            if (k == 1) {
              nr_iteratii = m;
              sigma_optim = sigma;
              System.arraycopy(xi, 0, x, 0, n);
            }
            if (k > 1 && m < nr_iteratii) {
              nr_iteratii = m;
              sigma_optim = sigma;
              System.arraycopy(xi, 0, x, 0, n);
            }
        }
        
        System.out.println("\n--------- Metoda Gauss - Seidel --------- \n");
        System.out.println("Parametrul optim: " + sigma_optim);
        System.out.println("Iteratii: " + nr_iteratii);
        afisareSolutie(x, n);
        verificaSolutie(matrix, x, b, n);
    }
    
 
    public static void GradientConjugat( double[][] matrix, double[] b, int n){
        double[] r = new double[n], v = new double[n]; 
        double[] ri1 = new double[n], vi1 = new double[n];
        double[] prod = new double[n];
        double[] x = new double[n];
        double[] y = new double[n];
        double ai = 0.0, ci = 0.0, cond = 0.0;
        int iteratii = 0;
        
        for (int i = 0; i < n; i++)
          x[i] = 0;
        
        System.arraycopy(b, 0, r, 0, n);
        System.arraycopy(r, 0, v, 0, n);
        
        do
        {
          for (int j = 0; j < n; j++){
						double sum = 0;
            for (int j1 = 0; j1 < n; j1++){
              sum += matrix[j][j1] * v[j1];
            }
            prod[j] = sum;
          }
          
          ai = (produsVectori(r, r, n)) / produsVectori(prod, v, n);
          
          for (int j = 0; j < n; j++)
            y[j] = x[j] + ai * v[j];
            
          for (int j = 0; j < n; j++){
            ri1[j] = r[j];
            double sum = 0;
            for (int i = 0; i < n; i++)
              sum += matrix[j][i] * v[i];
            ri1[j] -= ai * sum;
            }
            
            ci = produsVectori(ri1, ri1, n) / produsVectori(r, r, n);

            for (int j = 0; j < n; j++)
              vi1[j] = ri1[j] + ci * v[j];  
            
            cond = norma(y, x, n);
            System.arraycopy(ri1, 0, r, 0, n);
            System.arraycopy(vi1, 0, v, 0, n);
            System.arraycopy(y, 0, x, 0, n);

            iteratii++;
        } while (cond > EPSILON);
    		
    		System.out.println("\n--------- Metoda Gradientului Conjugat --------- \n");
        System.out.println("Iteratii: " + iteratii);
        afisareSolutie(y, n);
        verificaSolutie(matrix, y, b, n);
    }


    public static void Rotatii(double[][] matrix, double[] b, int n){
        double[][] x = new double[n][n];
        double[][] y = new double[n][n];
        double modul = 0.0;

        System.arraycopy(matrix, 0, x, 0, n);
        
        do
        {
        		int p = 0, q = 0;
            double max = 0.0;

            for (int i = 0; i < n - 1; i++)
              for (int j = i + 1; j < n; j++)
                if ( Math.abs( x[i][j] ) > max ){
                  p = i;
                  q = j;
                  max = Math.abs( x[i][j] );
            }
                
            
            double TETA = 0.0;
            if ( x[p][p] == x[q][q] )
              TETA = Math.PI / 4.0;
            else
              TETA = 0.5 * (Math.atan(( 2.0 * x[p][q] ) / ( x[p][p] - x[q][q] )));
            
            double cos = (double) Math.cos( TETA );
            double sin = (double) Math.sin( TETA );
            
            for (int i = 0; i < n; i++)
              for (int j = 0; j < n; j++)
                if (i != p && i != q && j != p && j != q)
                  y[i][j] = x[i][j];
          
            for (int j = 0; j < n; j++)
              if (j != p && j != q) {
                y[p][j] = y[j][p] = cos * x[p][j] + sin * x[q][j];
                y[q][j] = y[j][q] = (-1.0) * sin * x[p][j] + cos * x[q][j];
              }
            
            y[p][q] = y[q][p] = 0;
            y[p][p] = (cos * cos) * x[p][p] + 2.0 * (cos * sin) * x[p][q] + (sin * sin) * x[q][q];
            y[q][q] = (sin * sin) * x[p][p] - 2.0 * (cos * sin) * x[p][q] + (cos * cos) * x[q][q]; 
            
            System.arraycopy(y, 0, x, 0, n);

            
            double sum = 0.0;
            for (int i = 0; i < n; i++)
              for (int j = 0; j < n; j++)
                if (i != j)
                  sum += x[i][j] * x[i][j];
            
            modul = Math.sqrt(sum);
        
        } while (modul > EPSILON);
        
        System.out.println("\n-------- Metoda Rotatiilor --------\n");
        for (int i =0; i < n; i++) {
        	System.out.println(x[i][i]);
        }
    }


    public static void main(String[] args) {
        int n = 100;
        int p = 11;
        double[][] matrix = generateMatrix(n);
        double[] b = generateVector(n);

        Jacobi(matrix, b, n, 2);
       	GaussSeidel(matrix, b, n, p);
        GradientConjugat(matrix, b, n);
        Rotatii(matrix, b, n);
    }
}
