import java.util.*;
import java.io.*;

class Set implements Comparable<Set> {

    double x, y;

    public Set(){
        this.x = 0.0;
        this.y = 0.0;
    }

    public Set(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int compareTo(Set other) {
        if (Math.abs(this.x - other.x) > 0.001 || Math.abs(this.y - other.y) > 0.001 )
            return 1;
        return 0;
    }

    @Override
    public String toString() {
        return String.format("(%+.20f, %+.20f)", this.x, this.y);
    }
}

class Newton {

    double[][] matrix, invMatrix;
    ArrayList<Set> solutions;
    static double EPSILON = 1e-10;

    public Newton() {
        this.matrix = new double[2][2];
        this.invMatrix = new double[2][2];
        this.solutions = new ArrayList<>();
    }

    public double f1(Set s) {
        return 7.0 * ( s.x * s.x * s.x - s.y ) - 10.0 * s.x + 1.0;
    }

    public double f2(Set s) {
        return 8.0 * s.y * s.y * s.y - 11 * s.y + s.x - 1.0;
    }

    public double d_f1_d_x(Set s) {
        return 21.0 * s.x * s.x - 10.0;
    }

    public double d_f1_d_y(Set s) {
        return -7.0;
    }

    public double d_f2_d_x(Set s) {
        return 1.0;
    }

    public double d_f2_d_y(Set s) {
        return 24.0 * s.y * s.y - 11.0;
    }

    public double determinant() {
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
    }

    public void setMatrix(Set s) {
        this.matrix[0][0] = d_f1_d_x(s);
        this.matrix[0][1] = d_f1_d_y(s);
        this.matrix[1][0] = d_f2_d_x(s);
        this.matrix[1][1] = d_f2_d_y(s);
    }

    public void setInverse() {
        double det = determinant();

        invMatrix[0][0] =  matrix[1][1] / det;
        invMatrix[0][1] = -matrix[0][1] / det;
        invMatrix[1][0] = -matrix[1][0] / det;
        invMatrix[1][1] =  matrix[0][0] / det;
    }


    public Set getSolution(double x1, double x2) {
        Set X = new Set(x1, x2);
        Set aux = new Set();
        int n = 0;
        double norm = 0.0;

        do {
            n++;

            setMatrix(X);
            setInverse();

            double f1 = f1(X);
            double f2 = f2(X);

            double aux1 = X.x - (invMatrix[0][0] * f1 + invMatrix[0][1] * f2);
            double aux2 = X.y - (invMatrix[1][0] * f1 + invMatrix[1][1] * f2);

            aux = new Set(aux1, aux2);
            norm = Math.max(Math.abs(aux.x - X.x), Math.abs(aux.y - X.y));
            X = aux;
        } while (norm > EPSILON);

        return X;
    }

    public void firstOrAdd(Set other) {
        for(Set solution : solutions)
            if(other.compareTo(solution) == 0)
                return;
        solutions.add(other);
    }

    public void getSolutions() {
        double step = 0.2;

        for (double x0 = -2; x0 <= 2; x0 += step) {
            for (double y0 = -2; y0 <= 2; y0 += step) {
                Set solution = getSolution(x0, y0);
                firstOrAdd(solution);
            }
        }
    }

    public void printSolutions (){
        getSolutions();
        for (Set solution : solutions) {
            System.out.println(solution);
        }
    }
}

class Prob2{
    int n, m;
    double z, p, a, b;
    double[] x;
    ArrayList<Set> solutions;

    public Prob2(int n, int m, double a, double b){

        this.n = n;
        this.m = m;

        this.a = a;
        this.b = b;

        this.p = 0.0;
        this.z = 0.0;

        this.x = new double[this.n];
        this.x[0] = -1.0 / 3.0;
        this.x[1] = -1.0 / 5.0;
        this.x[2] = -1.0 / 10.0;
        this.x[3] = 0.0;
        this.x[4] = 1.0 / 10.0;
        this.x[5] = 1.0 / 5.0;
        this.x[6] = 1.0 / 3.0;

        this.solutions = new ArrayList<>();
    }

    public double function(double x){
        return 1.0 / (1.0 + 100 * x * x);
    }

    public void toJson(String methodName, String fname){
        try
        {   File file = new File (fname);
            PrintWriter writer = new PrintWriter(file);
            writer.print( methodName + " = ");
            writer.print("'{ ");
            writer.print( "\"method\":\"" + methodName  + "\",");
            writer.print( "\"m\":\"" + m  + "\",");
            writer.print( "\"sets\":[");
            int index = 0;
            for (Set s : solutions) {
                if (index == m-1)
                    writer.print(" {\"x\":\"" + s.x + "\", \"y\":\"" + s.y  + "\"}" );
                else
                    writer.print(" {\"x\":\"" + s.x + "\", \"y\":\"" + s.y  + "\"}," );
                index++;
            }
            writer.print("]}'\n");
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("File not Found;");
        }
    }

    public void printSolutions (){
        for (Set solution : solutions) {
            System.out.println(solution + " -> " + function(solution.x));
        }
    }
}

class Lagrange extends Prob2 {
    double[] y;

    public Lagrange(int n, int m, double a, double b){
        super(n, m, a, b);

        this.y = new double[this.n];
        for(int i = 0; i < this.n; i++)
            this.y[i] = function(this.x[i]);

    }

    public void getSolutions() {

        for(int k = 0; k < m; k++){
           this.z = this.a + (this.b - this.a) / (this.m - 1.0) * k;

            this.p = 0.0;
            for(int i = 0; i < this.n; i++) {
                double prod = 1.0;
                for(int j = 0; j < this.n; j++){
                    if( j != i)
                        prod *= (this.z - this.x[j]) / (this.x[i] - this.x[j]);
                }
                this.p += this.y[i] * prod;
            }
            solutions.add(new Set(this.z, this.p));
        }
        printSolutions();
    }

     public void toJson(){
        super.toJson("lagrange", "plot/lagrange.js");
     }
}

class Newton2 extends Prob2{

    public Newton2(int n, int m, double a, double b){

        super(n, m, a, b);
    }

    public void getSolutions(){
        for(int k = 0; k < m; k++) {
            this.z = this.a + ((this.b - this.a) / (this.m - 1)) * k;

            double[][] a = new double[n][n];

            for(int i = 0; i < n; i++) {
                a[0][i] = function(this.x[i]);
            }

            double[] c = new double[n];
            c[0] = a[0][0];

            for (int i = 1; i < n; i++) {
                for (int j = i; j < n; j++) {
                    a[i][j] = (a[i-1][j] - a[i-1][j-1]) / (this.x[j] - this.x[ j - i ]);
                }
                c[i] = a[i][i];
            }

            this.p = c[n-1];

            for (int i = n - 1; i >= 0; i--) {
                this.p = this.p * (this.z - this.x[i]) + c[i];
            }

            solutions.add(new Set(this.z, this.p));
        }

        printSolutions();
    }

    public void toJson(){
        super.toJson("newton", "plot/newton.js");
    }
}

class Interpolare {
    int n, m;
    double a, b, w, z, p;
    double[] x, y, h, a1, b1, c, v;

    public Interpolare(int n, int m, double a, double b) {
        this.n = n;
        this.m = m;
        this.a = a;
        this.b = b;
        this.w = functionPrim(this.a);
        this.z = functionPrim(this.b);

        this.x = new double[this.n];
        this.y = new double[this.n];
        this.h = new double[this.n];

        for(int i = 0; i < n ; i++) {
            this.x[i] = this.a + (this.b - this.a ) / (this.n - 1) * i;
        }

        for(int i = 0; i < n; i++) {
            this.y[i] = function(this.x[i]);
        }

        for (int i = 0; i < n - 1; i++) {
            this.h[i] = this.x[i + 1] - this.x[i];
        }
    }

    public double function(double x){
       return x * Math.sin(x) + (x * x + 4) * Math.exp(x) - Math.cos(x);
    }

    public double functionPrim(double x){
       return Math.sin(x) + x * Math.cos(x) + 2 * x * Math.exp(x) + (x * x + 4) * Math.exp(x) + Math.sin(x);
    }

    public void setABC(){
        this.a1 = new double[this.n];
        this.b1 = new double[this.n];
        this.c = new double[this.n];
        for (int i = 0; i <  n - 2; i++) {
            this.a1[i+1] = this.h[i+1] / (this.h[i] + this.h[i+1]);
            this.b1[i+1] = this.h[i] / (this.h[i] + this.h[i+1]);
            this.c[i] = (3 * this.h[i+1] * ( this.y[i + 1] - this.y[i])) / (this.h[i] * (this.h[i] + this.h[i+1])) + (2 * this.h[i] * ( this.y[i+2] - this.y[i+1])) / (this.h[i+1] * (this.h[i] + h[i+1]));

        }
    }

    public void determV(){
        this.v = new double[n];
        this.v[0] = this.w;
        this.v[1] = -18.968498;
        this.v[2] = 740.946173;
        this.v[3] = 3484.601811;
        this.v[n-1] = this.z;
    }

    public void getSolutions() {
        setABC();
        determV();

        double[] alfa, beta;
        alfa = new double[n];
        beta = new double[n];

        for(int i = 0; i < n - 1; i++) {
           alfa[i] = (3.0 / (this.h[i] * this.h[i])) * ( this.y[i+1] - this.y[i]) - (this.v[i+1] + 2 * this.v[i]) / this.h[i];
           beta[i] = (-2.0 / (this.h[i] * this.h[i]) * this.h[i]) * ( this.y[i+1] - this.y[i]) + (this.v[i+1] + this.v[i]) / this.h[i] * this.h[i];
        }

        for( int k = 0; k < m; k++) {
            double z1 = this.a + (this.b - this.a) / (this.m - 1) * k;
            double p = 0.0;
            for (int i = 1; i < n - 1; i++) {
                if( z1 >= this.x[i] && z1 <= this.x[i+1]) {
                    p = this.y[i] + this.v[i] * (z1 - this.x[i]) + alfa[i] * (z1 - this.x[i]) * (z1 - this.x[i]) + beta[i]  * (z1 - this.x[i]) * (z1 - this.x[i]) * (z1 - this.x[i]);
                }
            }
            System.out.println(z1 + " " + p);
        }

    }


}

public class Tema4 {
    private static Newton N;
    private static Lagrange L;
    private static Newton2 N2;
    private static Interpolare I;

    public Tema4(int m) {
        this.N = new Newton();
        this.L = new Lagrange(7, m, -1.5, 1.5);
        this.N2 = new Newton2(7, m, -1.5, 1.5);
        this.I = new Interpolare(5, m, 0.0, 2 * Math.PI);
    }

    public static void newton(){
        System.out.println("--------------- Prob 1: Newton ----------------\n");
        N.printSolutions();
        System.out.println("------------------ End Newton -----------------\n");
    }

    public static void lagrange(){
        System.out.println("---------------- Prob 2: Lagrange ---------------\n");
        L.getSolutions();
        L.toJson();
        System.out.println("------------------ End Lagrange -----------------\n");
    }

    public static void newton2(){
        System.out.println("---------------- Prob 2: Newton ---------------\n");
        N2.getSolutions();
        N2.toJson();
        System.out.println("------------------ End Newton -----------------\n");
    }

    public static void interpolare(){
        System.out.println("---------------- Prob 3: Interpolare ---------------\n");
        I.getSolutions();
        System.out.println("------------------ End Interpolare -----------------\n");
    }


    public static void main(String[] args) {
       Tema4 T = new Tema4(100);
       T.newton();
       T.lagrange();
       T.newton2();
       T.interpolare();
    }
}
