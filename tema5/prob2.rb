#!/usr/bin/ruby

EPSILON = 1e-10

def f y
	(y ** 3.0) / (Math.exp(y) - 1.0)
end

def integrala x, integ
	3.0 * (x ** -3) * integ
end

def main a, b, n, denominator
	s, err, k = 0.0, 0.0, 1.0
	loop do
#		puts "k: #{k}"
		h = (b - a) / k
#		puts "h: #{h}"
		sum = 0.0
		k.to_i.times { |i| sum += yield a, b, h, k, i + 1 }
		sigma = (h / denominator) * sum
#		puts "sigma: #{sigma}"
		err = (sigma - s).abs
		s = sigma
		k += 1
		break if err < EPSILON and k > n
	end
	puts integrala b, s
#	puts k
end

a = 0.0
b = 50.0
n = 1000

puts "Dreptunghi:"
main(a, b, n, 1.0) { |a, b, h, k, i| f(a + (2.0 * i - 1.0) * h / 2.0) }

puts "Newton - Cotes cu doua puncte:"
main(a, b, n, 2.0) { |a, b, h, k, i| f(a + (3.0 * i - 2.0) * h / 3.0) +
                       f(a + (3.0 * i - 1.0) * h / 3.0) }

puts "Newton - Cotes cu trei puncte:"
main(a, b, n, 3.0) { |a, b, h, k, i| 2.0 * f(a + (4.0 * i - 3.0) * h / 4.0) -
                       f(a + (2.0 * i - 1.0) * h / 2.0) +
                       2.0 * f(a + (4.0 * i - 1.0) * h / 4.0) }

puts "Newton - Cotes cu patru puncte:"
main(a, b, n, 24.0) { |a, b, h, k, i|	11.0 * f(a + (5.0 * i - 4.0) * h / 5.0) +
                            	f(a + (5.0 * i - 3.0) * h / 5.0) +
                              f(a + (5.0 * i - 2.0) * h / 5.0) +
                       				11.0 * f(a + (5.0 * i - 1.0) * h / 5.0) }

puts "Gauss cu doua puncte:"
main(a, b, n, 2.0) { |a, b, h, k, i| f(a + (2.0 * i - 1.0) * h / 2.0 - (b - a) /
													 	(2.0 * k * Math.sqrt(3))) +
                       			f(a + (2.0 * i - 1.0) * h / 2.0 +
                       			(b - a) / (2.0 * k * Math.sqrt(3))) }

puts "Gauss cu trei puncte:"
main(a, b, n, 18.0) { |a, b, h, k, i|  5.0 * f(a + (2.0 * i - 1.0) * h / 2.0 - (b - a) *
											 Math.sqrt(15) / (10.0 * k)) + 8.0 * f(a + (2.0 * i - 1.0) * h / 2.0) +
                       5.0 * f(a + (2.0 * i - 1.0) * h / 2.0 + (b - a) * Math.sqrt(15) /
                       	(10.0 * k)) }
