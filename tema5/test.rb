#!/usr/bin/ruby
require 'benchmark'

EPSILON = 1e-10

def f x
	((x**7) * Math.sqrt(1 - x**2)) / ((2 - x)**(6.5))
end

def main a, b, n, denominator
	s, err, k = 0.0, 0.0, 1.0
	loop do
#		puts "k: #{k}"
		h = (b - a) / k
#		puts "h: #{h}"
		sum = 0.0
		k.to_i.times { |i| sum += yield a, b, h, k, i + 1 }
		sigma = (h / denominator) * sum
#		puts "sigma: #{sigma}"
		err = (sigma - s).abs
		s = sigma
		k += 1
		break if err < EPSILON and k > n
	end
#	puts s
end


def main_complete a, b, n, denominator
	s, err, k = 0.0, 0.0, 1.0
	loop do
#		puts "k: #{k}"
		h = (b - a) / k
#		puts "h: #{h}"
		sum = 0.0
		k.to_i.times { |i| sum += f(a + i * h) + f(a + (i+1) * h) }
		sigma = (h / denominator) * sum
#		puts "sigma: #{sigma}"
		err = (sigma - s).abs
		s = sigma
		k += 1
		break if err < EPSILON and k > n
	end
#	puts s
end


a = -1.0
b = 1.0
n = 10

Benchmark.bm do |bm|
	bm.report { main(a, b, n, 2.0) {|a, b, h, k, i| f(a + (i - 1.0) * h) + f(a + i * h) } }
  bm.report { main_complete a, b, n, 2.0 }
end
