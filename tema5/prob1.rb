#!/usr/bin/ruby

EPSILON = 1e-10

def f x
	((x**7) * Math.sqrt(1 - x**2)) / ((2 - x)**(6.5))
end

def main a, b, n, denominator
	s, err, k = 0.0, 0.0, 1.0
	loop do
#		puts "k: #{k}"
		h = (b - a) / k
#		puts "h: #{h}"
		sum = 0.0
		k.to_i.times { |i| sum += yield a, b, h, k, i + 1 }
		sigma = (h / denominator) * sum
#		puts "sigma: #{sigma}"
		err = (sigma - s).abs
		s = sigma
		k += 1
		break if err < EPSILON and k > n
	end
	puts s
#	puts "k: #{k.to_i}, iteratii: #{ (0..k.to_i).inject(:+) }"
end

a = -1.0
b = 1.0
n = 10

puts "Trapez:"
main(a, b, n, 2.0) {|a, b, h, k, i| f(a + (i - 1.0) * h) + f(a + i * h) }


puts "Simpson:"
main(a, b, n, 6.0) {|a, b, h, k, i| f(a + (i - 1.0) * h) +
                       4 * f(a + (2.0 * i - 1.0) * h / 2.0) +
                       f(a +  i * h) }

puts "Newton:"
main(a, b, n, 8.0) {|a, b, h, k, i|  f(a + (i - 1.0) * h) +
                       3.0 * f(a + (3.0 * i - 2.0) * h / 3.0) +
                       3.0 * f(a + (3.0 * i - 1.0) * h / 3.0) +
                       f(a + i * h) }

puts "Boole:"
main(a, b, n, 90.0) {|a, b, h, k, i| 7.0 * f(a + (i - 1.0) * h) +
                       32.0 * f(a + (4.0 * i - 3.0) * h / 4.0) +
                       12.0 * f(a + (2.0 * i - 1.0) * h / 2.0) +
                       32.0 * f(a + (4.0 * i - 1.0) * h / 4.0) +
                       7.0 * f(a + i * h) }

puts "Gauss cu doua puncte:"
main(a, b, n, 2.0) {|a, b, h, k, i| f(a + (2.0 * i - 1.0) * h / 2.0 - (b - a) /
											(2.0 * k * Math.sqrt(3))) + f(a + (2.0 * i - 1.0) *
											h / 2.0 + (b - a) / (2.0 * k * Math.sqrt(3))) }

puts "Gauss cu trei puncte:"
main(a, b, n, 18.0) {|a, b, h, k, i|  5.0 * f(a + (2.0 * i - 1.0) * h / 2.0 - (b - a) *
												Math.sqrt(15) / (10.0 * k)) + 8.0 * f(a + (2.0 * i - 1.0) *
												h / 2.0) + 5.0 * f(a + (2.0 * i - 1.0) * h / 2.0 + (b - a) *
												Math.sqrt(15) / (10.0 * k)) }
