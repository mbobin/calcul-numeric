import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class Tema2 {
	static int BANANA = 200; // for scale
	public static void printMatrix(BigDecimal pascal[][], int n){

    for (int i = 1; i <= n ; i++) {
    	for (int j = 1; j <= n ; j++) {
       System.out.print(pascal[i][j] + " ");
    	}
       System.out.print("\n");
		} 
	}

	public static BigInteger factorial(int i, int n) {
	      BigInteger fact = new BigInteger("1");
	      for (; i <= n; i++) {
	          fact = fact.multiply(BigInteger.valueOf(i));
	      }
	      return fact;
	  }

	public static BigDecimal[][] trans(BigDecimal m[][], int n){
		BigDecimal tr[][] = new BigDecimal[n+1][n+1];

		for (int i=1; i<=n ; i++) {
			for (int j =1 ; j <= n ; j++) {
				tr[j][i] = m[i][j];				
			}
		}
		return tr;
	}

	public static BigDecimal[][] multiplyMatrix(BigDecimal m1[][], int n1, BigDecimal m2[][]){
		BigDecimal mRez[][] = new BigDecimal[n1+1][n1+1];
		for (int i=1; i<=n1 ; i++) {
			for (int j =1 ; j <= n1 ; j++) {
				BigDecimal sum = new BigDecimal(0);
				for(int k =1; k <= n1; k++)
					sum = sum.add(m1[i][k].multiply(m2[k][j]));  
				mRez[i][j] = sum;
			}
		}
		return mRez;
	}

	public static BigDecimal[][] generateMatrix(int n, int p){
	  BigDecimal m[][] = new BigDecimal[n+1][n+1];
    for (int i = 1; i <= n ; i++) {
    	for (int j = 1; j <= n ; j++) {
    		BigDecimal nfact = new BigDecimal(factorial(i, p+j-1));
    		BigDecimal nminkfact = new BigDecimal(factorial(1, p+j-i));

    		m[i][j] = nfact.divide(nminkfact);
    	}
    }
    return m;
  }

  public static BigDecimal[] sumOnLine(BigDecimal m[][], int n){
  	BigDecimal column[] = new BigDecimal[n+1];
  	for(int i=1; i<=n;i++){
  		BigDecimal sum = new BigDecimal(0);
  		for (int j=1; j<=n ;j++ ) {
  			sum = sum.add(m[i][j]);
  		}
  		column[i] = sum;
  	}
  	return column; 
  }

  public static void verifica(BigDecimal A[][], int n, BigDecimal x[], BigDecimal b[]){
  	
  	System.out.println("\n------ Verificare ------");
  	for (int i = 1; i <= n; i++) {
  	 	BigDecimal sum = new BigDecimal(0);
  	 	for(int j = 1; j <= n; j++){
  	 		sum = sum.add(A[i][j].multiply(x[j]));
  	 	}
  	 	sum = sum.subtract(b[i]);
  	 	System.out.println(i + ": " + sum);
  	 } 
  		System.out.println("\n------ Verificare END ------");
  	 	System.out.println();

  }

  public static void LU(int n, int p, BigDecimal A[][], BigDecimal b[]){
    BigDecimal L[][] = new BigDecimal[n+1][n+1];
    BigDecimal U[][] = new BigDecimal[n+1][n+1];

    for (int i=1 ; i<=n ;i++ ) {
    	for (int j = 1; j<=n ;j++ ) {
			  L[i][j] = new BigDecimal(0);
    		U[i][j] = new BigDecimal(0);    		
    	}
    }

    for (int i=1; i<=n;i++) {
    	L[i][1] = A[i][1];
    }
//    printMatrix(L, n);
//    System.out.println();
    
    U[1][1] = new BigDecimal(1);
    for (int i = 2; i<=n ; i++) {
    	U[1][i] = A[1][i].divide(L[1][1],BANANA, RoundingMode.HALF_UP);
    }

//    printMatrix(U, n);
//    System.out.println();

    for(int j=1; j<=n; j++){
    	U[j][j] = new BigDecimal(1);
    }

    for(int k = 2; k <= n; k++){
    	
    	for (int i = k; i <= n ;i++ ) {
    		BigDecimal sum = new BigDecimal(0);
    		for(int p1 = 1; p1 < k; p1++){
    			sum = sum.add(L[i][p1].multiply(U[p1][k]));
    		}
    		L[i][k] = A[i][k].subtract(sum);	
    	}

    	for (int j = k+1; j <= n; j++) {
    			BigDecimal sum = new BigDecimal(0);
    			for(int p1 = 1; p1 < k; p1++){
    				sum = sum.add(L[k][p1].multiply(U[p1][j]));
    			}
    			U[k][j] = (A[k][j].subtract(sum)).divide(L[k][k],BANANA, RoundingMode.HALF_UP);
    	}
    }
/*
    System.out.println("L:");
    printMatrix(L, n);
    System.out.println();

    System.out.println("U:");
		printMatrix(U, n);
    System.out.println();

    System.out.println("\nL*U:");
    printMatrix( multiplyMatrix(L,n,U), n); // A != LU ?! why, Java? WHYYY?!?!?!
    System.out.println();
*/
    //  L*y = b;

    BigDecimal y[] = new BigDecimal[n+1];

//    System.out.println("b:");
//    for (int i=1; i<=n;i++ ) {
//    	System.out.print(b[i] + " ");
//    }
//    System.out.println();

    for(int i = 1; i <= n; i++ ){
    	BigDecimal sum = new BigDecimal(0);
    	for(int k = 1; k < i; k++){
    		sum = sum.add(L[i][k].multiply(y[k]));
    	}
    	y[i] = (b[i].subtract(sum)).divide(L[i][i],BANANA, RoundingMode.HALF_UP);
    }

//    System.out.println("y:");    
//    for(int i=1; i<=n; i++){
//   		System.out.print(y[i] + " ");
//   	} 
//   	System.out.println();

    // Find X and don't ask y.
    BigDecimal x[] = new BigDecimal[n+1];
    for(int i = n; i >= 1; i-- ){
    	BigDecimal sum = new BigDecimal("0");
    	for(int k = i+1; k<=n; k++){
    		sum = sum.add(U[i][k].multiply(x[k]));
    	}
    	x[i] = y[i].subtract(sum);
    }

   // Print 'solution' :))
    System.out.println("Solutiile X:");
   for(int i=1; i<=n; i++){
   	System.out.println("X[ " + i + " ]: " + x[i]);
   } 
   System.out.println();
   	verifica(A, n, x, b);

  }
	

	public static BigDecimal sqrt(BigDecimal A, final int SCALE) {
    BigDecimal TWO = new BigDecimal("2");
    BigDecimal x0 = new BigDecimal("0");
    BigDecimal x1 = new BigDecimal(Math.sqrt(A.doubleValue()));
    while (!x0.equals(x1)) {
        x0 = x1;
        x1 = A.divide(x0, SCALE, RoundingMode.HALF_UP);
        x1 = x1.add(x0);
        x1 = x1.divide(TWO, SCALE, RoundingMode.HALF_UP);
    }
    return x1;
	}


  public static void Cholesky(int n, int p, BigDecimal A[][], BigDecimal b[]){
  	BigDecimal L[][] = new BigDecimal[n+1][n+1];

  	for (int j = 1; j <= n; j++) {
  		BigDecimal sum = new BigDecimal(0);
  		for(int k = 1; k < j; k++){
  			sum = sum.add(L[j][k].multiply(L[j][k]));
  		}
  		L[j][j] = sqrt(A[j][j].subtract(sum), BANANA);

  		for (int i = j+1; i <= n; i++) {
  		  BigDecimal	sum1 = new BigDecimal(0);
  			for(int k = 1; k < j; k++){
  				sum1 = sum1.add(L[i][k].multiply(L[j][k]));
  			}
  			L[i][j] = (A[i][j].subtract(sum1)).divide(L[j][j], BANANA, RoundingMode.HALF_UP);
				
  		}
  	}

  	// L * y = b 
  	BigDecimal y[] = new BigDecimal[n+1];
  	for (int i = 1; i <= n; i++ ) {
  		BigDecimal sum = new BigDecimal(0);
  		for (int k = 1; k < i; k++) {
  			sum = sum.add(L[i][k].multiply(y[k]));
  		}
  		y[i] = (b[i].subtract(sum)).divide(L[i][i], BANANA, RoundingMode.HALF_UP);
  	}

  	BigDecimal x[] = new BigDecimal[n+1];
		
		for (int i = n; i >= 1; i--) {
			BigDecimal sum = new BigDecimal(0);
			for (int k = i+1; k <= n ; k++) {
				sum = sum.add(L[k][i].multiply(x[k]));
			}
			x[i] = (y[i].subtract(sum)).divide(L[i][i], BANANA, RoundingMode.HALF_UP);  		
		}  	

	// Print 'solution' :))
    System.out.println("Solutiile X:");
   for(int i=1; i<=n; i++){
   	System.out.println("X[ " + i + " ]: " + x[i]);
   } 
   System.out.println();
   	verifica(A, n, x, b);

  }



  public static void QR(int n, int p, BigDecimal A[][], BigDecimal b[]){
  	BigDecimal Q[][] = new BigDecimal[n+1][n+1];
  	BigDecimal R[][] = new BigDecimal[n+1][n+1];

  	BigDecimal sum = new BigDecimal(0);
  	for(int  i = 1; i <= n; i++){
  		sum = sum.add(A[i][1].multiply(A[i][1]));
  	}
  	R[1][1] = sqrt(sum, BANANA);

  	for(int  i = 1; i <= n; i++){
  		Q[i][1] = A[i][1].divide(R[1][1], BANANA, RoundingMode.HALF_UP);
  	}

  	for (int k = 2; k <= n; k++) {
  		// R(j,k)
  		for (int j = 1; j < k; j++) {
  		  sum = new BigDecimal(0);
  			for (int i = 1; i <= n; i++) {
  				sum = sum.add(A[i][k].multiply(Q[i][j]));
  			}
  			R[j][k] = sum;
  		}

  		// R(k,k)
  		BigDecimal sum1 = new BigDecimal(0);
  		BigDecimal sum2 = new BigDecimal(0);

  		for(int i = 1; i <= n; i++){
  			sum1 = sum1.add(A[i][k].multiply(A[i][k]));
  		}
  		for(int i = 1; i < k; i++){
  			sum2 = sum2.add(R[i][k].multiply(R[i][k]));
  		}
//			Real.setExactPrecision(150);
//System.out.println(Real.valueOf(2).sqrt())
  		BigDecimal t1 = (sum1.subtract(sum2));
  		R[k][k] = sqrt(t1, BANANA);

  		// Q(i,k)
  		for(int i = 1; i <= n; i++){
  			sum = new BigDecimal(0);
  			for(int j = 1; j < k; j++ ){
  				sum = sum.add(R[j][k].multiply(Q[i][j]));
  			}
  			Q[i][k] =  (A[i][k].subtract(sum)).divide(R[k][k], BANANA, RoundingMode.HALF_UP);
  		}
		}

		BigDecimal QTrans[][] = trans(Q, n);	
		BigDecimal y[] = new BigDecimal[n+1];

		for(int i = 1; i <= n; i++){
			sum = new BigDecimal(0);
			for (int j = 1; j <= n; j++) {
				sum = sum.add(Q[j][i].multiply(b[j]));
			}
			y[i] = sum;
		}

		BigDecimal x[] = new BigDecimal[n+1];

		x[n] = y[n].divide(R[n][n], BANANA, RoundingMode.HALF_UP);

		for(int i = n-1; i >= 1; i--){
			sum = new BigDecimal(0);
			for(int j = i+1; j <= n; j++){
				sum = sum.add(R[i][j].multiply(x[j]));
			}
			x[i] = (y[i].subtract(sum)).divide(R[i][i],BANANA, RoundingMode.HALF_UP);
		}
	// Print 'solution' :))
    System.out.println("Solutiile X:");
   for(int i=1; i<=n; i++){
   	System.out.println("X[ " + i + " ]: " + x[i]);
   } 
   System.out.println();
   	verifica(A, n, x, b);
  }
  


  public static void main(String []args) {
    int n = 20;
    int p = 50;

    BigDecimal aLaP[][] = generateMatrix(n,p);
//    System.out.println("A(p)");
//    printMatrix(aLaP,n);
    BigDecimal aLaPTrans[][] = trans(aLaP, n);
//    System.out.println("\nA(p) transpus:");
//    printMatrix(aLaPTrans,n);
    BigDecimal A[][] = multiplyMatrix(aLaPTrans,n,aLaP); 
//    System.out.println("\nA = A(p)trans * A(p)");
//    printMatrix(A, n);
//    System.out.println();
    BigDecimal b[] = sumOnLine(aLaPTrans,n);

// Call LU method
    LU(n, p, A, b);

// Call Cholesky method
    Cholesky(n, p, A, b);

// Call QR
		QR(n, p, A, b);
	}
}