EPSILON = 1e-13
SOLUTIONS_WANTED = 5

def function x
	3 - Math.exp(Math.sin(2*(x*x)) + Math.cos(x*x))
#  (-1) * Math.exp(Math.sin(2*x*x) + Math.cos(x*x))*(4*x*Math.cos(2*x*x)-2*x*Math.sin(x*x))
end


def bisectie a, b
	p = (a + b)/2.0
	while (function p).abs >= EPSILON do
		if function(a) * function(p) > 0 then
			a = p
		else
			b = p
		end
		p = (a + b)/2.0
	end
	puts "#{p} -> #{function(p)}"
	return p
end

puts "Rezultat Metoda Bisectiei:"

solutions = 0
step = 0.01
i = 0.0

while solutions < SOLUTIONS_WANTED
	100.times do |j| 
		if function(i + j*step) * function(i+(j+1)*step) < 0 
			solutions += 1 if bisectie(i + j*step, i+(j+1)*step)
			break if solutions >= 5
		end
	end			
	i += 1
end

