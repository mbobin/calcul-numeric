EPSILON = 1e-13
SOLUTIONS_WANTED = 5

def function x
	3 - Math.exp(Math.sin(2*(x*x)) + Math.cos(x*x))
#	 (-1) * Math.exp(Math.sin(2*x*x) + Math.cos(x*x))*(4*x*Math.cos(2*x*x)-2*x*Math.sin(x*x))
end


def secanta a,b
	start = a
	final = b

	while (start-final).abs > EPSILON
		xn = final - function(final) * ((final-start)/(function(final) - function(start)))
		if function(xn).abs < EPSILON
			puts "#{xn} -> #{function(xn)}"
			return xn
		else 
			function(xn) * function(start) < 0 ? final = xn : start = xn
		end
	end
end

puts "Rezultat metoda secantei:"

solutions = 0
step = 0.01
i = 0.0

while solutions < SOLUTIONS_WANTED
	100.times do |j| 
		if function(i + j*step) * function(i+(j+1)*step) < 0 
			solutions += 1 if secanta(i + j*step, i+(j+1)*step)
			break if solutions >= 5
		end
	end			
	i += 1
end 