EPSILON = 1e-13
SOLUTIONS_WANTED = 5

def function x
	 3 - Math.exp(Math.sin(2*(x*x)) + Math.cos(x*x))
end

def function_1 x
	(-1) * Math.exp(Math.sin(2*x*x) + Math.cos(x*x))*(4*x*Math.cos(2*x*x)-2*x*Math.sin(x*x))
end

def deriv x
	return (function(x+EPSILON) - function(x-EPSILON))/(EPSILON * 2);
end

def newton x
	while function(x).abs > EPSILON
		x = x - function(x)/deriv(x)
	end
	puts "#{x} -> #{function x}"
	return x
end

puts "Rezultate metoda Newton: "

solutions = 0
step = 0.01
i = 0.0

while solutions < SOLUTIONS_WANTED
	100.times do |j| 
		if function(i + j*step) * function(i+(j+1)*step) < 0 
			solutions += 1 if newton(i + j*step)
			break if solutions >= 5
		end
	end			
	i += 1
end


puts function_1 1
puts deriv 1