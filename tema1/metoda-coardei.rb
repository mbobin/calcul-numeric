EPSILON = 1e-13
SOLUTIONS_WANTED = 5

def function x
	3 - Math.exp(Math.sin(2*(x*x)) + Math.cos(x*x))
#  (-1) * Math.exp(Math.sin(2*x*x) + Math.cos(x*x))*(4*x*Math.cos(2*x*x)-2*x*Math.sin(x*x))
end

def coarda a, b
	x = a - function(a) * ((b-a)/(function(b) - function(a)))
	while function(x).abs > EPSILON
		x = a - function(a) * ((x-a)/(function(x) - function(a)))
	end
	puts "#{x} -> #{function x}"
	return x
end

puts "Rezultate metoda Coardei"

solutions = 0
step = 0.01
i = 0.0

while solutions < SOLUTIONS_WANTED
	100.times do |j| 
		if function(i + j*step) * function(i+(j+1)*step) < 0 
			solutions += 1 if coarda(i + j*step, i+(j+1)*step)
			break if solutions >= 5
		end
	end			
	i += 1
end